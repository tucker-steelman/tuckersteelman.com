import Vue from 'vue'
import VueRouter from 'vue-router'
import ProfilePage from '@/views/ProfilePage.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Profile',
        component: ProfilePage
    }
]

const router = new VueRouter({
    routes
})

export default router
