export default {
    namespaced: true,

    state: {
        badge: {
            title: 'Software Developer',
            tag: 'Dedicated to building the best damn products available.'
        },
        contact: {
            email: {
                label: 'Email',
                value: 'tucker.steelman@gmail.com',
                link: 'mailto:tucker.steelman@gmail.com'
            },
            bitbucket: {
                label: 'Bitbucket',
                value: 'https://bitbucket.org/tucker-steelman/',
                link: 'https://bitbucket.org/tucker-steelman/'
            }
        }
    }
}
