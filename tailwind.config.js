module.exports = {
    purge: false,
    theme: {
        extend: {},
        screens: {
            'xs': '462px',
            'sm': '640px',
            'md': '768px',
            'lg': '1024px',
            'xl': '1280px'
        },
        colors: {
            'portrait-white': '#f0f0f0'
        }
    },
    variants: {},
    plugins: [],
}
